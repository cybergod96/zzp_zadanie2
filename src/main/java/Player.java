import java.util.ArrayList;
import java.util.List;

public class Player {
    public Player(){
        this.trainings = new ArrayList<Training>();
    }

    public Player(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.trainings = new ArrayList<Training>();
    }

    private String name;
    private String surname;
    private List<Training> trainings;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean addTraining(Training t){
        trainings.add(t);
        return trainings.contains(t);
    }

    public Training getTraining(int index){
        return trainings.get(index);
    }

    public Double  getAverage(){
        Double sum = 0d;
        for(Training t: trainings){
            sum += t.getAverage();
        }
        return sum/ trainings.size();
    }

}
