import java.util.ArrayList;
import java.util.List;

public class Training {
    private String description;
    private List<Integer> notes;
    public Training(){
        this.notes = new ArrayList<Integer>();
    }

    public Training(String description) {
        this.description = description;
        this.notes = new ArrayList<Integer>();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean addNote(Integer n){
        notes.add(n);
        return notes.contains(n);
    }

    public Integer getBestNote(){
        Integer max = 0;
        for(Integer i: notes){
            if(i>max)
                max = i;
        }
        return max;
    }

    public Double getAverage(){
        Integer sum = 0;
        for(Integer n : notes){
            sum +=n;
        }
        return Double.valueOf(sum/notes.size());
    }
}
