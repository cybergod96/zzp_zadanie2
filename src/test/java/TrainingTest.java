import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TrainingTest {
    private Training training = new Training();
    @Test
    public void  trainingIsCreated(){
        assertTrue(training instanceof Training);
    }

    @Test
    public void addNoteToTraining(){
        assertTrue(training.addNote(5));
    }

    @Test
    public void getBestNote()
    {
        training.addNote(5);
        training.addNote(4);
        training.addNote(3);
        training.addNote(2);
        assertEquals(Integer.valueOf(5) ,training.getBestNote());
    }

    @Test
    public void getTrainingAvarege(){
        training.addNote(5);
        training.addNote(4);
        training.addNote(3);
        training.addNote(2);
        assertEquals(Double.valueOf((5+4+3+2)/4), training.getAverage());
    }
}
