import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import org.junit.Assert.*;
import org.junit.Test;

public class PlayerTest {
    private Player player = new Player();

    @Test
    public void playerIsCreated(){
        assertTrue(player instanceof Player);
    }

    @Test
    public void addTrainingToPlayer(){
        Training training = new Training("Biegnij!");
        assertTrue(player.addTraining(training));
    }

    @Test
    public void getTrainingFromPlayer(){
        player.addTraining(new Training());
        Training t = player.getTraining(0);
        assertNotNull(t);
    }

    @Test
    public void getPlayerAverage(){
        Training t1 = new Training("t1");
        Training t2 = new Training("t2");
        t1.addNote(4);
        t1.addNote(2);
        t2.addNote(4);
        t2.addNote(2);

        player.addTraining(t1);
        player.addTraining(t2);
        assertEquals(Double.valueOf(3),player.getAverage());
    }

}
